---
title: Jatuhnya Bulan Ramadan 2018
date: 2018-04-24T05:39:19+00:00
author: Sekar
url: jatuhnya-bulan-ramadan-2018.html
---

<img src="/images/B823387220Z.1_20170606141630_000_GAE1T5VV7.2_Super_Portrait.jpg" alt="" />

Saatnya adalah tahun ketika umat Islam di seluruh dunia menahan diri dari makan dan minum dari fajar hingga senja. ‘Sawm’, atau puasa selama bulan Ramadhan, adalah salah satu hal paling penting yang dilakukan seorang Muslim dalam hidupnya, karena ini adalah salah satu dari 5 rukun Islam.

## Apa itu Ramadan?

Meskipun tidak makan atau minum merupakan aspek utama Ramadhan, ada tujuan yang jauh lebih penting. Sudah saatnya bagi kita untuk memperbaiki diri sebagai Muslim dan manusia, menahan diri dari tindakan-tindakan berbahaya, dan mendapatkan tempat kita di Jannah di akhirat. Ini adalah bulan ketika setiap Muslim menetapkan patokannya sendiri bahwa ia harus bertujuan untuk menciptakan kembali selama sisa hidupnya. Kami menurunkan tatapan kami, menahan lidah kami kembali, memberikan banyak amal, berdoa, membaca, dan menghabiskan waktu sebanyak mungkin melakukan hal-hal yang menyenangkan pencipta kita dan membawa kita lebih dekat kepada-Nya.

## Kapan Ramadan 2018?

Kalender Ramadhan 2018 diperkirakan akan dimulai pada malam hari Rabu, 16 Mei 2018.
  
Itu kemudian diprediksi akan berakhir pada malam Kamis, 14 Juni, 2018.

## Kalender Islam

Kalender Islam berisi 12 bulan, yang kesembilan bulan suci Ramadhan. Bulan-bulan ini tidak bertepatan dengan kalender ilmiah. Sebaliknya, mereka didasarkan pada penampakan bulan baru, atau bulan sabit.

Meskipun setiap bulan dalam kalender Islam dimulai dan berakhir dengan penampakan seperti itu, Anda mungkin akan mendengar lebih banyak tentangnya sebelum Ramadhan. Ini karena umat Islam di seluruh dunia bersemangat menunggu kedatangannya sehingga kami dapat yakin bahwa bulan suci telah tiba.

Ini berarti bahwa meskipun tanggal yang diprediksikan pada awal Ramadhan 2018 adalah 16 Mei, itu mungkin benar-benar berakhir pada tanggal 17. Demikian pula, penampakan bulan menandai akhir Ramadhan, dan awal Idul Fitri. Ini juga bisa ditunda satu hari, atau bahkan datang satu hari sebelumnya.

## Metode alternatif Menentukan Awal Ramadan

Meskipun penampakan bulan adalah cara yang paling umum dan kredibel di mana komunitas Muslim di seluruh dunia menentukan awal dan akhir Ramadhan, ada juga beberapa cara lain di mana komunitas tertentu melakukannya.
  
Salah satu metode tersebut adalah penerimaan penampakan di negara lain (paling sering Arab Saudi) bahkan jika para sarjana di negara mereka sendiri belum dikonfirmasi untuk hari itu. Metode lain adalah menghitung tanggal melalui metode “Hijriri”. Hijriah menjadi kata Islam selama setahun, itu hanya berarti bahwa prediksi dibuat sebelumnya (mirip dengan kalender ilmiah) dan diikuti. Kemajuan teknologi telah membuat ini lebih dapat diandalkan dalam beberapa hari terakhir.